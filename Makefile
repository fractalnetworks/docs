DOCKER=docker
DOCKER_COMPOSE=docker compose
IMAGE=registry.gitlab.com/fractalnetworks/fractal-mosaic:docs
MKDOCS=mkdocs
GIT=git

# build docs. should be run inside the shell started with `make shell`.
docs:
	$(MKDOCS) build

up:
	$(DOCKER_COMPOSE) up

down:
	$(DOCKER_COMPOSE) down

logs:
	$(DOCKER_COMPOSE) logs

# build docker image for shell
build:
	$(DOCKER) build . -t $(IMAGE)

# launch shell
shell: build
	$(DOCKER) run -it --rm -v $(PWD)/..:/mosaic --workdir /mosaic/docs --user $(shell id -u) $(IMAGE)

# clean stuff
clean:
	$(GIT) clean -fdX

.PHONY: docs
