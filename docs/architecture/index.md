# HomeServer Architecture

## Fractal HomeServer
Fractal HomeServer is a cooperative self-hosting platform. It enables you to easily self-host applications by yourself, with friends or co-workers. With Fractal HomeServer, you can leverage your existing devices to run self-hosted applications or support existing Fractal HomeServer self-hosted applications. Applications and devices are organized into groups. These groups can consist of just your devices or any number of users you invite.


<figure markdown>
![](../media/homeServerSpaceArch.png){width="300"}
<figcaption>Root HomeServer space layout</figcaption>
</figure>
The cloud-like nature of Fractal HomeServer allows for the redundancy of applications. This means if a device running an applications fails, another device in its group can take its place with minimal disruption to the application.

In essence, a Fractal HomeServer is a collection of groups meant to provide a way to organize your self-hosted applications and devices to meet your needs or the needs of those within your HomeServer community.

## Groups
Users create Groups to enable cooperative self-hosting in a decentralized manner, free of externally hosted services (Amazon, Digital Ocean, Azure, etc). Users add devices to Groups and deploy Applications across those devices. Devices coordinate with other devices within its Group to maintain the state of the groups' Applications. In this context, Application state refers to various metadata such as the current device running the application, the health of the application and the Application's data.
<figure markdown>
![Group layout in Matrix](../media/homeServerGroupArch.png){width="300"}
<figcaption>Group layout in Matrix</figcaption>
</figure>

A Group can has Applications, Devices, and Members (User). Groups are represented by Matrix Spaces. Spaces in the context of matrix are a way to group Matrix Rooms and users together. Devices coordinate through a dedicated Matrix Rooms in real-time. Room events, such as chat messages, are sequentially logged and appear to the user in the order in which they occurred. Devices in the group communicate with each other using this sequential log. For this reason, the sequential log is crucial to ensuring that tasks are not executed more than once unless desired.

 When a group is created, the group owner can add any number of their devices and invite other users to the group using their Matrix IDs. Once an invitation is accepted, the invited member can then add any number of their devices to help contribute to applications in the group. Applications can be installed through the command line (```homeserver app install vaultwarden --group "Group 1" --device "Raspberry Pi"```) and in the future, be installable through a graphical user interface.
