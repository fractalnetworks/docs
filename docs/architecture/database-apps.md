# Fractal Database Apps Documentation

## Key Components

- **Poetry (Python Packaging):** Package and Dependency Manager, cross-platform, supporting many Python versions.
  
- **Django:** A high-level Python web framework that encourages rapid development and clean, pragmatic design.
  
- **Synapse (Matrix Protocol):** Forms the backbone of decentralized communication within Fractal Database Apps.
  
- **TaskIQ:** Manages task execution and coordination, ensuring smooth operation of Fractal Database services.

## Poetry

Poetry is a powerful tool for managing Python dependencies and packaging. It provides a simple and intuitive interface for managing project dependencies, handling versioning, and packaging your applications for distribution. With support for cross-platform development and compatibility with multiple Python versions, Poetry streamlines the development and deployment process for Fractal Database Apps.

### Poetry `pyproject.toml`

The `pyproject.toml` file is the heart of a Poetry project, containing metadata and configuration information. It specifies project dependencies, build system requirements, and additional metadata such as project name, version, and authors. By leveraging Poetry's `pyproject.toml`, developers can easily manage project dependencies, define package metadata, and ensure consistency across environments.

Learn more about Poetry and `pyproject.toml` [here](https://python-poetry.org/docs/).

## Fractal Database App Discovery

Fractal Database Apps are automatically discovered based on whether the package exposes a `"fractal.database.app"` entrypoint. This allows for seamless integration and discovery of apps within the Fractal Database ecosystem.

## Apps

Fractal Database Apps are Django applications designed from the ground up with Fractal Database in mind. They can be easily created using the `fractal db create` command, which generates a Django app as an installable Python package. This simplifies the process of creating and deploying Fractal Database Apps, allowing developers to focus on building innovative solutions.

## Sharing Apps

Once a developer is satisfied with their Fractal Database App, they can upload it to any desired location, such as PyPI or a Matrix server. Fractal Database also supports uploading apps as Docker Container tarballs to Matrix, resulting in an MXC URI that users can easily share and download.

## Downloading Apps

Users can download Fractal Database Apps via their MXC URI. Once downloaded, users can run the app as a standalone Docker container, providing flexibility and ease of use.

This documentation provides an overview of the key components and functionalities of Fractal Database Apps, empowering developers to build and share innovative applications within the Fractal Database ecosystem.
