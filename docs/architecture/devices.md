---
title: Devices
---

Beginning your self-hosted journey begins with the Docker container that runs and manages your self-hosted applications. A Fractal Device is what runs your applications, creates the storage volumes that encrypt and replicate your
application's data, and maintains a connection to the Hive Backend in order to receive orchestration commands.

## Device Container Structure

A Device is a [Python Base Image Container](https://hub.docker.com/_/python) that also has the Docker CLI installed
inside of it. Having the Docker CLI installed allows the Device to run Docker commands on your behalf. In order to do
this, a Docker socket needs to be mounted into the device.

<div class="center-table" markdown>

### Directories in Device Container

|  ***Path***  | ***Description***                                    |
| :--------: | :------------------------------------------------: |
| **/apps**  | Where your app's Docker Compose files are located. |
| **/code**  | Source code for the Device                         |
| **/tmp**   | Docker Storage Plugin UNIX Socket mounted here     |

</div>

### Why Mount the Docker Socket?

In order to launch and manage your applications, the Device requires access to the host machine's Docker socket. The
Docker socket allows the Device to manage the Fractal Storage Docker Plugin and to create and manage your application's
Docker containers. The Storage Plugin is what is responsible for encrypting and replicating your data. **Fractal does not
and will never collect any data on Docker or your system other than the apps launched by the Device.** If preferred, you
can also simply run a Device in an isolated **Docker** environment. *Instructions for doing so are coming soon.*

## Getting Started

### System Requirements

- [Docker (or Docker Desktop)](https://docs.docker.com/get-docker/)

### Adding a Device

Getting started is as simple as adding and running a device. Adding a device is a relatively simple process. First, you
must give your account a passphrase that your devices will use to encrypt your data (storage volume encryption keys).
Setting a passphrase for your device ensures that Fractal Networks does not have the ability to read your data. Hive only
stores the salted and hashed passphrase in order to confirm that you use the same passphrase for all of your devices.
This is for your convenience and so that you can migrate your apps to any of the devices running in Fractal HomeServer.

!!! warning "Set a Strong Device Passphrase"
    ***The Device passphrase is used when generating the private keys that encrypt your data so make sure you use a
    secure passphrase.***


After submitting the device passphrase, the next step is to optionally name your Device and run the Device. The Docker
command snippet will appear pre-populated with everything the Device requires to start, including the name and secret
passphrase you created. *If you do not specify a name, the Device will use the hostname of the machine it runs on.*
**The Docker run command includes the secret Device passphrase you have chosen, so treat it as a secret.**


You're now ready to launch your device! Simply copy the Docker command and paste it into the terminal of the machine
that you want to use as a Fractal HomeServer device.


**Your device will not show up in the Console until you run the Docker command.** After running the Docker command, you'll
know your device is ready to run applciations when you can see your device in the Device list on the right side of the
Console with a green health circle next to its name. Once your device is reported online in the Console. You're ready to
start running applications on your Device.


## What is my Device Doing?

On initial startup, your Device will install the
[Fractal Storage Docker Plugin](https://docs.fractalnetworks.co/developers/storage/), then connect to the Hive Backend.
Upon connecting to the backend, your device is ready to being running various tasks.

### Periodic Tasks

Your device periodically:

- Reports the apps it's currently aware of running. Reporting currently running apps is what allows the Hive Backend to
determine the current health of the application.
- Checks and reports the current state of the storage volumes for Fractal applications. This report includes the public
key of the app volume, and the last time your data was saved (last replicated timestamp). Doing so allows Hive to inform
you of the current state of your application's data.
- If opted into automatic updates, then your device reports its version to the Hive Backend. The Hive backend then
ensures that the Device's version is up to date.

### Starting an App

The Device's main task is running applications. When you choose to install an app from the app catalog to a device, the
device:

1. Receives a start command which contains metadata about the app. This includes a domain that the app will be acccesible
from once starting (ex. hasty-hopper.fractalnetworks.co).
1. Creates encryption keys for the app. One for the app's data, and one for the app's link that allows for public
connectivity.
1. Creates Docker volumes using the encryption keys generated above.
1. Starts the app using the volumes created above.
1. Upon successfully starting the app, report the app as running, periodically.

### Application Failover

As long as your devices are using the same device passphrase, then you have the ability to failover an application from
one device to another. When you choose to another device to run an application on in the Console, the failover process
is as follows:

1. Your first device will stop the app, ensuring that all recent data is replicated.
1. The second device will being the start process by first decrypting the passphrase encrypted storage keys that are
stored in the backend.
1. Your storage volumes are created based on the encryption keys that were decrypted in the previous step. Upon volume creation, the volumes will being downloading all of the data uploaded by the first device.
1. Once the download is complete, your application is started with all of the data intact.

Your application should now be running identically to how it was running on the previous device.

For more information on how Hive handles failover, refer to the
[Hive Backend Documentation](https://docs.fractalnetworks.co/HomeServer/hive).

### A Note on Generated Storage Private Keys

The device encrypts the generated storage private keys with the device passphrase you set when launching the device and
sends these encrypted keys to be stored in the Hive Backend. Storing these encrypted keys allows other devices you add
to be able to decrypt the storage keys (as long as you launch them with the same Device passphrase). Without the device
passphrase, these keys cannot be decrypted. This means that even if the Hive Backend database is compromised,
*your data is safe and secure.*

### In Summary

Your device periodically reports:

- Currently running Fractal applications.
- Storage volume metadata for currently running Fractal applications
  - Includes last replicated timestamp (how recent was your data saved).
  - Public Keys for app volumes so that Hive can report the current replication state of your volume.
- Current version (if automatic updates are enabled)

These tasks are what allow the Device to manage your self-hosted applications.

<hr />

<div class="center-table" markdown>

### Supported Device Commands

|    ***Command Type***      | ***Description***                                                                       |
| :------------------------: | :-------------------------------------------------------------------------------------: |
| **Scheduler Actions**      | Should your device start or stop an app.                                                |
| **System Commands**        | ex. checking disk space.                                                                |
| **App Specific Commands**  | App configuration commands based off of actions taken from the Console.                 |
| **Upgrade Commands**       | Run [Watchtower](https://hub.docker.com/r/containrrr/watchtower) to update your device. |

</div>

!!! info "A Note on Device and Specific Commands"
    Fractal ***never*** runs arbitrary commands on your device. Each command is templated, and if any user input is required
    for the command, it will be escaped to guarantee no arbitrary code execution.
