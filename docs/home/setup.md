# Quick Start
<!--
What dependencies are required for the following to work
fractal-database?
fractal-database-matrix?
selfhosted-gateway?
-->

Prerequisites

- Docker or Docker for Desktop (Linux, macOS or Windows)

- Minimum 8GB RAM

- Modern AMD64 or AARCH64 processor

- Familiar with command-line

Get started with a Fractal Networks hosted, custodial alpha deployment [console.fractalnetworks.co](https://console.fractalnetworks.co).

The following is the process for setting up the Homeserver

aquire the homserver bash commands by
```
curl get.homeserver.org | bash
```
### Homesever Root Setup

```
$homesever init
```

When HomeServer is initalized, it creates a device membership, a group which by default is the "Personal Room", a replication channel, and replication logs. The device membership gives your device access to the personal room. The replication channel acts as a backup which stores representation logs in the local device. These representation logs gives the user a list of object history which allows all information from the group to be stored and replicated easily.

# <img src="/home/homeInit.PNG" alt="Sections" style="width: 70%;" />

### Gateway Setup
>   ssh into accessible IP

```
~#homeserver gateway init
```

```
~#logout
```

### Add Gateway to a Group

```
$homeserver gateway add <SSH URL of the gateway> <database_name> <ssh port>
```

### Installing Synapse to a group

```
$homeserver app install synapse <group_name> <domain_name> <path_to_compose_file>
```

### Register New User

```
$homeserver register <name-of-new-matrix-id>
```


```
$homeserver login <matrix-id>
```

### Replicate Homeserver Data to Synapse

```
$homeserver replicate to <weblocation-of-synapse>
```

### Link the Gateway to Matrix
>   $homeserver gateway list

>   shows names of active gateways

```
$homeserver gateway register <gateway-name>
```
