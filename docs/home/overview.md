# HomeServer

HomeServer is an open source, non-custodial personal computing platform designed to make self-hosting easy and reliable. It combines an App Store with simple container orchestration, storage, and connectivity solutions based on popular open source tools. HomeServer simplifies collaborative self-hosting through the Matrix Synapse protocol, allowing users to form groups and organize applications collectively. Users can contribute devices to ensure redundancy and reliability of applications, powered by the Fractal Database. With HomeServer, you can create your own secure and resilient cloud infrastructure, tailored to your specific needs and preferences. Connectivity is enhanced with Fractal Gateway and WireGuard, enabling secure external access. Additionally, TaskIQ integrates with the Matrix protocol to handle task management efficiently, using workers to execute tasks kicked into Synapse rooms.

> HomeServer introduces several high-level technical concepts such as the Fractal Network, RPoVPN, portable self-hosted applications, collaborative-hosting, transitive digital trust, forkable organizations among others. See the glossary for an overview of HomeServer's technical concepts.

HomeServer enables multi-device self-hosted deployment of popular free software applications, automatic updates, backups with point-in-time recovery, and a robust distributed ingress connectivity solution making it possible to self-host reliably from home, with IPv6 or from behind a firewall or CGNAT (carrier-grade NAT).

HomeServer is designed to be simple enough that you can take it apart and figure out how it works. We call it *manual-included software*. We built HomeServer to bootstrap a sustainable software ecosystem and escape vendor lock-in once and for all. We eschew technological centralization and walled gardens.

HomeServers consist of four main parts:

# <img src="/home/fourSections.PNG" alt="Sections" style="width: 100%;" />

HomeServer is not yet perfect. If you want to come on this journey with us, we hope that you will have a good experience and help us improve that experience for others yet to come.

### Key Components

- **Fractal Database:** Powers data storage and replication within the HomeServer environment.
- **Django:** Python web framework.
  - **Fixtures:** Facilitate pre-loading data into the database for testing and development.
  - **Signals:** Enable decoupled applications to get notified when certain actions occur elsewhere in the application.
- **Synapse (Matrix Protocol):** Forms the backbone of decentralized communication within HomeServer.
- **TaskIQ:** Manages task execution and coordination, ensuring smooth operation of HomeServer services.
- **Fractal Gateway:** Facilitates public connectivity for apps by accepting HTTP requests and forwarding them to Fractal Links. It maintains a WireGuard VPN Tunnel with Fractal Links to ensure secure communication.

### Features:

- **Decentralized Collaboration:** Form groups to collaborate on self-hosted applications seamlessly. Additionally, HomeServer provides granular control over access and permissions, ensuring data security and privacy.
- **Redundancy and Reliability:** Contribute devices to ensure continuous availability of applications. HomeServer's failover mechanisms and automated backups further enhance reliability and data integrity.
- **Powered by Fractal Database:** Ensures data integrity and scalability across the HomeServer environment. Fractal Database's robust replication and synchronization capabilities provide users with peace of mind and seamless access to their data.
- **Extensible Application Ecosystem:** Supports a wide range of self-hosted applications powered by Docker. HomeServer's plugin architecture and community-driven development model enable users to extend and customize their cloud infrastructure effortlessly.
- **Simplified Deployment and Management:** Intuitive interfaces and automated scripts make deployment and management hassle-free. With HomeServer, you can focus on building and innovating, rather than worrying about infrastructure management.


