# Packaging your Application for HomeServer

With Fractal Homeserver we offer a low cost and effective to self-hosted applications.
HomeServer applications are built on Docker containers. An application is thus defined as a
docker-compose file. Here's an example of how the application docker-compose looks like for
Vault Warden:

```yaml
version: '3.9'
services:
  vaultwarden:
    image: vaultwarden/server:latest
    volumes:
      - fractal-app-volume:/data
    healthcheck:
      test: curl localhost:80
      interval: 10s
      timeout: 10s
      retries: 5
    restart: unless-stopped
    expose:
      - "vaultwarden:80"

volumes:
  fractal-app-volume:
  # fractal-app-volume:
  #   external: true
  #   name: "vaultwarden-data"
```

In this file, you can find two unusual things:

- The expose, which opens the application to be used by Fractal HomeServer and Gateway.

- The volumes, which are declared as external volumes. This is where the Fractal Storage volumes
  are used, which implement automatic snapshotting and with that replication in an end-to-end
  encrypted manner.

## Instructions

If you are able to build a Docker image with your application (or a few Docker images, for example
if and when you need external tools, such as databases), then it is very easy to convert it into
an application that can run on HomeServer.

What you have to keep in mind is that your application will receive HTTP connections from the link
container, not HTTPS. HomeServer works best if you let the link container handle all of the TLS termination
for you. It uses `Caddy` to do so, which can be configured to support a lot of modern protocols if you
need, such as HTTP/2 or WebSockets.

The only think you need to worry about is making sure that all of your application state is kept in
volumes, so that it can be snapshotted and replicated.

When you publish updates for your application, you must make sure that it will work with data from old
versions of the app. This is for example accomplished by running migrations automatically at startup
to make sure that the database is in working order.


