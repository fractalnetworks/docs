# Contributing For Developers

<br>

## Process

If you want to contribute and don't know where to start, here is a step-by-step for a complete lifecycle, from idea to deployment.

### 1. Initial Idea and Discussion

- **Start a Discussion**: If you have an idea not already in discussion or issues, open up on [Github](https://github.com/fractalnetworksco), reach out on [Discord](https://discord.gg/SAJdnXMG), or check out our [Matrix channel](https://matrix.to/#/#fractal:ether.ai).

### 2. Getting Familiar

- **Understand HomeServer**: Familiarize yourself with Fractal Database, Django, Fixtures and Signals, Matrix Protocol (Synapse), TaskIQ, Fractal Gateway, and WireGuard.

### 3. Submitting Enhancements or Bugs

- **Agree on Solution**: Discuss and agree on the solution with the team. Clear communication is key.

### 4. Implementation

- **Code Implementation**: Write code following agreed-upon solutions and guidelines. Maintain clear communication throughout the implementation phase.

### 5. Review and Deployment

- **Code Review**: Maintain communication and address feedback.
- **Deployment**: A core team member will deploy to production once merged.

### 6. Post-Merge

- **Sync Your Fork**: Keep your local repository up-to-date with the main branch.
