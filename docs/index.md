# <img src="home/fractal-wordmark.svg" alt="Fractal Networks" style="height: 1.7ex;" /> HomeServer
---

## Welcome to the Fractal HomeServer's documentation 

Check out our overview video below or [Quick Start](home/setup.md) to get started!

Fractal HomeServer makes self-hosting accessible and survivable. In addition to empowering individuals to self-host, with Fractal HomeServer, users can form groups to self-host applications collectively. Group participants contribute devices to ensure redundancy and reliability of applications and services. Fractal HomeServer enables users to create their own secure and resilient cloud infrastructure, tailored to the specific needs and preferences of the individual or group. Global connectivity is enabled by the Fractal Gateway to ensures secure access. Additionally, HomeServer includes a Djagno based Software Development Kit that integrates seemlessly with the Matrix protocol to handle distributed task management, real-time block and object level data replication with a batteries included message oriented architechture of workers used to execute tasks dispatched via Matrix rooms. 


<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/9b36cece24bb4d20b43326dbfb1aa46b?sid=1db39122-73e2-4e7a-a2ff-a40b4adc66ed" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>