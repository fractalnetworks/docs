---
title: Applications
---

# Applications

The Fractal HomeServer App Catalog comes with a small collection of **Featured Services**. For now, supporting a small
selection of applications means that we can focus on improving the reliability of the system.

Drop by our [Matrix channel](https://matrix.to/#/#fractal:ether.ai) or open an issue on
[GitHub](https://github.com/fractalnetworksco/app-catalog/issues) if you'd like us to add your favorite self-hosted
applications to HomeServer!

## Migrating An Existing App

Fractal HomeServer applications are simply [Docker Compose](https://docs.docker.com/compose/) files. Therefore, migrating your
existing applications to take advantage of HomeServer's automated backups, secure connectivity, and rescheduling
capabilities is simple if you are already familiar Docker. Visit the developer documentation for more information
on how to [migrate existing applications to HomeServer](../developers/porting.md).

## Installing Apps

When logged into the Console, Fractal HomeServer's *Featured Services* are presented.

If you select any one of the *Featured Services* and click the *Deploy* button, a dialog will open
asking you to select a device to deploy the application on.

Feel free to select or add any device you like. Once you have selected a device, the application will be started on your
chosen device. At this point, your application is displayed inside the Console.

!!! note "Fractal Cloud Device Offering"
    The *Fractal Cloud Device* is a *potential offering* where we will run your
    application in the cloud, if you want to try HomeServer without setting up your own device. These devices are not currently
    provided, but if you would like the ability to run one, leave a message in our
    [Matrix channel](https://matrix.to/#/#fractal:ether.ai) or open an issue on
    [GitHub](https://github.com/fractalnetworksco/app-catalog/issues).


> Most applications will require some first time setup which varies between the applciation

## Configuring Apps

Installed applications in the Console offer a variety of actions that can be taken:

- Uninstall
  - Removes the application from your device. This also removes the data for the app.
- Reschedule
  - Marks the app to be rescheduled to another device of your choosing.
- Application Specific Configuration
  - Specific app configuration that can be applied to the app. [See Element App Configuration](../developers/porting.md)
