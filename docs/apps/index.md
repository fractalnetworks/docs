### [Element](https://github.com/element-hq)
:   Group Messaging

### [Photo Prism](https://github.com/photoprism)
:   Photo and Video Storage

### [Vault Warden](https://github.com/dani-garcia/vaultwarden/pkgs/container/vaultwarden)
:   Password Management