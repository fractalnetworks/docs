# Glossary

### App (Database)
:   A type of Database that houses various applications within the Fractal Database ecosystem.

### Database
:   A collection of user-defined ReplicatedModels.

### Device (ReplicatedModel)
:   Represents a physical device, such as a computer, within the Fractal Database framework.

### DeviceMembership (ReplicatedModel)
:   Represents a device's affiliation with a specific Database, detailing its role and permissions.

### DurableOperation
:   Local tasks within the Fractal Database that are guaranteed to be executed and stored persistently.

### Fractal Link
:   The link that connects the Fractal Gateway to a device

### Group (Database)
:   A structured Database that organizes devices and applications, facilitating collaborative self-hosting.

### ReplicationChannel (ReplicatedModel)
:   A pathway within the Fractal Database for synchronizing data between replicated models across various devices.

### ReplicationLog (DurableOperation)
:   A record created whenever a UserDefinedModel is instantiated, tracking changes and updates for consistency.

### ReplicatedModel
:   A fundamental data structure within Fractal Database designed to be duplicated and synchronized across multiple locations.

### Representations (DurableOperations)
:   Different forms or versions of DurableOperations that ensure consistent task execution and data replication.

### Service (Database)
:   A specialized Database designed to manage and replicate services across different devices and locations.

### Task
:   An executable function within the Fractal Database system, managed by TaskIQ and executed by workers to maintain database integrity and functionality.

### UserDefinedModel (ReplicatedModel)
:   A customizable ReplicatedModel created and managed by users to fit specific application needs.

### Volumes
:   The logical area of the disk. Serves as a container for the file system and provides structure for accessing data.
